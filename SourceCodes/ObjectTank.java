package tank.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

/*
 * ------------------------------------------------------------------------
* 
*  author  : Harri Isom�ki
*  
*  Class   : ObjectTank
*  
* ------------------------------------------------------------------------
* 
*  When user press " START GAME " button, this starts tank object.
*  Tank begins to move forward on the screen. 
*	  
*  Every time when tank object takes step, it send x/y- coordinates to
*  CollisionDetector by calling synchronized method checkCollision.
*
*  If tank object has collides with house object, position of tank is
*  changed by reversing tank.
*
*  Position of tank is counted with following formula :
*
*        x = x + incrementX     
*        y = y + incrementX *tan(tankAngle)
*
*-------------------------------------------------------------------------
*
* Tank is drawn with help of polygons. At first tank chassis is drawn,
* then turret and finally tank gun.
*
*
* To draw tank box with Polygon class tank object has to know all 4
* x/y-coordinates of box ( xc1/yc1,  xc2/yc2,  xc3/yc3,  xc4/yc4).
*
* Their position are counted with formula below.
* 
* 
*   yc1 = 50 * Math.sin(Math.PI/8.57 + tankAngle);    
*   xc1 = 50 * Math.cos(Math.PI/8.57 + tankAngle);
*	
*   yc2 = 50 * Math.sin(Math.PI-Math.PI/8.57 + tankAngle);
*   xc2 = 50 * Math.cos(Math.PI-Math.PI/8.57 + tankAngle);
*	
*   yc3 = 50 * Math.sin(Math.PI + Math.PI/8.57 + tankAngle);
*   xc3 = 50 * Math.cos(Math.PI + Math.PI/8.57 + tankAngle);
*	
*   yc4 = 50 * Math.sin(2*Math.PI-Math.PI/8.57 + tankAngle);
*   xc4 = 50 * Math.cos(2* Math.PI-Math.PI/8.57 + tankAngle);
*
* -----------------------------------------------------------------------
*
*       TANK BOX    
* 
*
*
*             xc4,yc4  ------------------------------.    xc1,yc1
*                      !                           . !
*                      !                         .   !
*                      !                       .     !  
*                      !                     .       !
*                      !                   .         !
*                      !                 . tankAngle !
*                      !          Xm,Ym .............!
*                      !                             !
*                      !                             !
*                      !                             !
*                      !                             !
*                      !                             !
*             xc3,yc3  -------------------------------   xc2,yc2
*
*
*
*  position xc1/yc1 is counted with formula below. 
*
*      
*      yc1 = 50 * Math.sin(Math.PI/8.57 + tankAngle);    
*      xc1 = 50 * Math.cos(Math.PI/8.57 + tankAngle);
*
*
*  50 is length between coordinates Xm/Ym and xc1/yc1. 
*
*  To count position  xc1/yc1  tank object has to know only
*  length 50, coordinates  Xm/Ym and tankAngle in advance.
*
*  When tankAngle is increased, then positions of all four
*  x/y-coordinates are changed --> tank box is rotating.
*
*-------------------------------------------------------------------------
*/


public class ObjectTank extends ObjBasicMoveableClass implements Runnable
{
	
    private Polygon chassis,turret,gun;
    private boolean isTankMoving = true;
    protected Thread tankThread;
    private double tankAngle,turretAngle = 0,x,y,velX = 1, velY = 1;
	
    private double xc1,xc2,xc3,xc4,yc1,yc2,yc3,yc4;
    private double xt1,xt2,xt3,xt4,yt1,yt2,yt3,yt4;
    private double xg1,xg2,xg3,xg4,yg1,yg2,yg3,yg4;
	

	
    public ObjectTank(double x, double y, int idNumber, Color color,
		   ObjBasicCollisionDetector objBasicCollisionDetector) 
    {
	super(x, y, idNumber, color, objBasicCollisionDetector);
		
	this.x = x;
	this.y = y;		
    }
	
	

	
    public void start() 
    {
	tankThread = new Thread(this);
	tankThread.start();
    }
	




	
     public double getGunX(){return xg1;}
     public double getGunY(){return yg1;}
     public double getGunAngle(){return turretAngle; }
	
     public void rotateTank(double tankAngle){this.tankAngle = tankAngle ;} 
     public void rotateTurret(double turretAngle) {this.turretAngle = turretAngle ; }	
	
     public void stopTank() { isTankMoving = false ;}	
     public void startTank(){ isTankMoving =true ;}
	
	
	
	
     private void move() 
     {
	if(tankAngle > 2*Math.PI){ tankAngle = tankAngle - 2*Math.PI;}
	if(tankAngle < -2*Math.PI){ tankAngle = tankAngle + 2*Math.PI;} 
		
		
	if(0 < tankAngle && tankAngle < Math.PI/2 ){ velX =1; }		
	if(Math.PI/2 < tankAngle && tankAngle < 1.5*Math.PI ){ velX = -1; }		
	if(1.5 *Math.PI < tankAngle && tankAngle < 2*Math.PI ){ velX = 1;}					

	if(-2*Math.PI < tankAngle && tankAngle < - 1.5*Math.PI/2 ){ velX =1; }	
	if(-1.5*Math.PI < tankAngle && tankAngle < - Math.PI/2 ){ velX = -1; }
	if(-Math.PI/2 < tankAngle && tankAngle < 0 ){ velX =1; }
		
		
        double tempX;     
	if( isTankMoving){tempX = velX ;}else {tempX = 0;}
		
        x = x +tempX;		
	velY =tempX * Math.tan(tankAngle);		
	
	if( velY > 2){velY = 2;}
	if( velY < -2){velY = -2;}		
		
	y = y + velY;	
     }
	



	
     public void checkCollisionDetected()
     {
         if(this.objBasicCollisionDetector.checkCollision(x,y,idNumber))
         {
	     y = y +10;
	 }
      }
	
	
	
	
       @Override
       public void run() 
	{
	   Thread thisThread = Thread.currentThread();
		
	   while(tankThread == thisThread)
	   {
	       move();			
               checkCollisionDetected();
			
	       try 
	       {
		  Thread.sleep(33);
	       }
               catch (InterruptedException e) 
			{
            	System.out.println("tank loop_" + e.getMessage() + "---" + e.getStackTrace() );
	     }
	   }		
	}

	
	
	
	
	public void paint(Graphics g)
	{
	    g.setColor(Color.RED);
	    g.fillOval((int)x,(int)y,11,11);
		
		
//--------------------------- tank chassis ---------------------------------
		
		
	    yc1 = 50 * Math.sin(Math.PI/8.57 + tankAngle);    
	    xc1 = 50 * Math.cos(Math.PI/8.57 + tankAngle);
		
	    yc2 = 50 * Math.sin(Math.PI-Math.PI/8.57 + tankAngle);
	    xc2 = 50 * Math.cos(Math.PI-Math.PI/8.57 + tankAngle);
		
	    yc3 = 50 * Math.sin(Math.PI + Math.PI/8.57 + tankAngle);
	    xc3 = 50 * Math.cos(Math.PI + Math.PI/8.57 + tankAngle);
		
	    yc4 = 50 * Math.sin(2*Math.PI-Math.PI/8.57 + tankAngle);
	    xc4 = 50 * Math.cos(2* Math.PI-Math.PI/8.57 + tankAngle);
		
	    yc1 = yc1 + y;
	    yc2 = yc2 + y;
	    yc3 = yc3 + y;
	    yc4 = yc4 + y;
		
		
	    xc1 = xc1 + x;
	    xc2 = xc2 + x;
	    xc3 = xc3 + x;
	    xc4 = xc4 + x;
	
	    yc1 = (int)Math.round(yc1) ;
	    yc2 = (int)Math.round(yc2) ;
	    yc3 = (int)Math.round(yc3) ;
	    yc4 = (int)Math.round(yc4) ;
		
	    xc1 = (int)Math.round(xc1) ;
	    xc2 = (int)Math.round(xc2) ;
	    xc3 = (int)Math.round(xc3) ;
	    xc4 = (int)Math.round(xc4) ;
		
	   chassis = new Polygon();
		
	   chassis.addPoint((int)xc1,(int)yc1);
	   chassis.addPoint((int)xc2,(int)yc2);
	   chassis.addPoint((int)xc3,(int)yc3);
	   chassis.addPoint((int)xc4,(int)yc4);
		
		
//--------------------------- tank turret  -------------------------------------
		
		
	   yt1 = 22.4 * Math.sin(Math.PI/14+ turretAngle);    
	   xt1 = 22.4 * Math.cos(Math.PI/14+ turretAngle);
		
	   yt2 = 15 * Math.sin(Math.PI-Math.PI/4 + turretAngle);
	   xt2 = 15 * Math.cos(Math.PI-Math.PI/4 + turretAngle);
		
	   yt3 = 15 * Math.sin(Math.PI + Math.PI/4 + turretAngle);
	   xt3 = 15 * Math.cos(Math.PI + Math.PI/4 + turretAngle);
		
	   yt4 = 22.4 * Math.sin(2*Math.PI-Math.PI/14+ turretAngle);
	   xt4 = 22.4 * Math.cos(2* Math.PI-Math.PI/14+ turretAngle);
		
	   yt1 = yt1 + y;
	   yt2 = yt2 + y;
	   yt3 = yt3 + y;
	   yt4 = yt4 + y;
		
		
	   xt1 = xt1 + x;
	   xt2 = xt2 + x;
	   xt3 = xt3 + x;
	   xt4 = xt4 + x;
		
	   yt1 = (int)Math.round(yt1) ;
	   yt2 = (int)Math.round(yt2) ;
	   yt3 = (int)Math.round(yt3) ;
	   yt4 = (int)Math.round(yt4) ;
		
	   xt1 = (int)Math.round(xt1) ;
	   xt2 = (int)Math.round(xt2) ;
	   xt3 = (int)Math.round(xt3) ;
	   xt4 = (int)Math.round(xt4) ;
		
	   turret = new Polygon();
		
	   turret.addPoint((int)xt1,(int)yt1);
	   turret.addPoint((int)xt2,(int)yt2);
	   turret.addPoint((int)xt3,(int)yt3);
	   turret.addPoint((int)xt4,(int)yt4);
		
		
//--------------------------- tank gun -----------------------------------------
		
		
	    yg1 = 40 * Math.sin(Math.PI/65+ turretAngle);    
	    xg1 = 40 * Math.cos(Math.PI/65+ turretAngle);
				
     	    yg2 = 4 * Math.sin(Math.PI-Math.PI/4 + turretAngle);
	    xg2 = 4 * Math.cos(Math.PI-Math.PI/4 + turretAngle);
			
	    yg3 = 4.8 * Math.sin(Math.PI + Math.PI/4 + turretAngle);
	    xg3 = 4.8 * Math.cos(Math.PI + Math.PI/4 + turretAngle);
			
	    yg4 = 40 * Math.sin(2*Math.PI-Math.PI/65+ turretAngle);
	    xg4 = 40 * Math.cos(2* Math.PI-Math.PI/65+ turretAngle);
				
	    yg1 = yg1 + y;
	    yg2 = yg2 + y;
	    yg3 = yg3 + y;
	    yg4 = yg4 + y;			
				
	    xg1 = xg1 + x;
	    xg2 = xg2 + x;
	    xg3 = xg3 + x;
	    xg4 = xg4 + x;
			
	    yg1 = (int)Math.round(yg1) ;
	    yg2 = (int)Math.round(yg2) ;
	    yg3 = (int)Math.round(yg3) ;
	    yg4 = (int)Math.round(yg4) ;
				
	    xg1 = (int)Math.round(xg1) ;
	    xg2 = (int)Math.round(xg2) ;
	    xg3 = (int)Math.round(xg3) ;
	    xg4 = (int)Math.round(xg4) ;
				
	    gun = new Polygon();
				
     	    gun.addPoint((int)xg1,(int)yg1);
    	    gun.addPoint((int)xg2,(int)yg2);
	    gun.addPoint((int)xg3,(int)yg3);
	    gun.addPoint((int)xg4,(int)yg4);
						
		
//--------------------------- draw polygons -----------------------------------------		
		
	    g.setColor(Color.green);
	    g.fillPolygon(chassis);
		
	    g.setColor(Color.black);
	    g.fillPolygon(turret);
		
	    g.setColor(Color.black);
	    g.fillPolygon(gun);
	}
	
}
