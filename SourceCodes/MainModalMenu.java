package tank.game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

/*
* ------------------------------------------------------------------------
* 
*  author  : Harri Isom�ki
*  
*  Class   : MainModalMenu
*  
*  -----------------------------------------------------------------------
*     
* In init()-method in MainClass checkPassWord()-method is executed. 
* This method creates a modal MainModalMenu using JDialog class.
* Menu implemented with Swing components.
*
* User gives identification id and password. Modal menu is now disposed.
* Id and password are now inspected. 
*
* If validation was not successful, a new modal menu is created with
* text "Please try again".
* 
* If user clicks cancel button, user is directed to StartPage.html-page.
*
*/



public class MainModalMenu extends JDialog implements ActionListener
{
	
    protected boolean pwSuccess =true ;	
    protected boolean buttonStatus = true;	
    private JButton okButton,cancelButton;
	
    protected JTextField username ;
    protected JPasswordField password;	
    protected JTextArea tArea;
	
	
    public MainModalMenu(Frame frame,boolean pwSuccess)
    {
	super(frame,"Welcome to play tank game",true);		
	this.pwSuccess = pwSuccess;		
	this.setSize(1200,780);
		
	username = new JTextField(9);
	password = new  JPasswordField(9) ;
		
		
	tArea = new JTextArea(34,70);	 
		
	Color color =new Color(250,250,210);
		
	tArea.setBackground(color);
	tArea.setForeground(Color.black);
		
	tArea.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		 
		
		 
		
        String text1 = "\n\n\n This game starts when you click  START GAME button at "
				
	+ " lower left corner in game screen.Tank begins to move forward "
				
				     
	+ "\n\n You can stop the tank by pressing ARROW UP  key"
	+ "\n You can start the tank by pressing ARROW DOWN   key  "
				     
				     
	+ "\n\n You can change the direction to left  by pressing ARROW LEFT  key"
	+ "\n You can change the direction to right  by pressing  ARROW RIGHT key"
				     
        + "\n\n You can rotate the turret to left by pressing  W key"
        + "\n You can rotate the turret to right by pressing Q "
			     
        + "\n\n You can fire the gun by pressing SPACE "
        + "\n Your task is to destroy all the houses on the screen "
                    
                     
				     
        + "\n\n The tank and the houses are implemented with polar coordinates, because"
        + "  my final goal is to change these 2D  "
        +  "\n objects into 3D objects in the future.  "
                   
        + "\n\n\n\n\n Please give id and password.  "
        + "\n\n Server start up may take more than 30 seconds, so be patient   ";
				    
				
		
				      
		
        String text2 = "\n\n\n  Error in user or password  "
		               + "\n\n  Please try again ";
		
		
		
	if(pwSuccess)
	{ 
	    tArea.setEditable(false);
	    Font font = new Font("SansSerif",Font.PLAIN,14);	 
	    tArea.setFont(font);
	    tArea.setText(text1);		
	}
	else
	{
		
	     tArea.setEditable(false);
	     Font font = new Font("SansSerif",Font.BOLD,14);	 
	     tArea.setFont(font);
	     tArea.setForeground(Color.red);
	     tArea.setText(text2);
	}
		
		
		
	JPanel north = new JPanel();
	north.setLayout(new FlowLayout());
	north.add(tArea);
	this.add(north);
		
	JPanel center = new JPanel();
	center.setLayout(new FlowLayout());		
	center.add(new Label("User :"));
	center.add(username);		
	center.add(new Label("Password :"));
	center.add(password);
	this.add(center);
		
		
		
	JPanel south = new JPanel();
	south.setLayout(new FlowLayout());
	this.add(south);		
		
	south.add(okButton = new JButton("okButton"));
	okButton.addActionListener(this);		
	south.add(cancelButton = new JButton("cancelButton"));
	cancelButton.addActionListener(this);		
		
		
	BorderLayout f = new BorderLayout();
	f.setHgap(1);
	f.setVgap(1);		
	this.setLayout(f);
				
		
	this.add(north,BorderLayout.NORTH);
	this.add(center,BorderLayout.CENTER);
	this.add(south,BorderLayout.SOUTH);
		
		
	Dimension d = this.getToolkit().getScreenSize();		
	this.setLocation(d.width/300, d.height/300);		
	this.setVisible(true);
  }
	
	

	@Override
	public void actionPerformed(ActionEvent ae) 
	{
	   if(ae.getSource() == okButton)
	   {
	       buttonStatus = true;
	       setVisible(false);
	   }
	       else if(ae.getSource() == cancelButton)
	   {
	       buttonStatus = false;
	       setVisible(false);
	   }	
	}
	
	
	
	public boolean getButtonStatus() { return buttonStatus; }
	

}
