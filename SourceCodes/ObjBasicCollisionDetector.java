package tank.game;

/*
* ------------------------------------------------------------------------
* 
*  author  : Harri Isom�ki
*  
*  Class   : ObjBasicCollisionDetector
*  
* ------------------------------------------------------------------------
*     
*  Every time when ammunition or tank object takes step, they send their
*  x/y- coordinates to CollisionDetector by calling synchronized method
*  checkCollision.
*
*  Received x/y- coordinates are compared with x/y- coordinates of house
*  objects.
*
*  If ammunition object has collided with house object, explosion is 
*  generated.
*  If tank object has collided with house object, position of tank is
*  changed.
*
*--------------------------------------------------------------------------
*/

public class ObjBasicCollisionDetector 
{
    private ObjBasicClass[] objectRegister;
    protected MainClass mainClass;
    private boolean collisionResult = false;
	
    private int collisionHitCounter;
	
	
    public ObjBasicCollisionDetector(ObjBasicClass[] objectRegister,MainClass mainClass)
    {
	this.objectRegister = objectRegister;
	this.mainClass = mainClass;
		
	collisionHitCounter =0;
    }
	
	
     public synchronized boolean checkCollision(double x, double y, int idNumber)
     { 
	 int source = idNumber;
	 collisionResult = false;		
		
		
	 int  length = 10;
	 for(int i=0; i < length ;i++)
	 {	    
	    double objX =  objectRegister[i].getObjectCoordinate_x();
	    double objY =  objectRegister[i].getObjectCoordinate_y();	        
	        
	        
	    
	    if(!objectRegister[i].hasCollisionHappened && (objectRegister[i].idNumber>1))
	    {
	    	double distance = ((x-objX) *( x-objX)) +((y-objY) *( y-objY));
	    	distance = Math.sqrt(distance);	    	
	    	
	    	if(distance < 25 && (!(source ==1)))
	    	{
	    	   objectRegister[i].generateExplosion();	    		
	    	   collisionResult = true;	    	
	    		
	    	   collisionHitCounter =collisionHitCounter +1;
     	     	   mainClass.generateExplosionSound();
	    		
	    	}
	    	
	    	
	    	double tankDistanceX =(x-objX)*(x-objX);
	        tankDistanceX =Math.sqrt(tankDistanceX);
	    	
	    	double tankDistanceY =(y-objY)*(y-objY);
	    	tankDistanceY =Math.sqrt(tankDistanceY);
	    	 
     	        if(tankDistanceX <70 && tankDistanceY < 30 && ((source == 1)) )	
	    	{
	    	    collisionResult = true;
	    	}
	     }
	 }
		return collisionResult;
       }

	
	
     public String getHits()
     {   		
	String ctr =Integer.toString(collisionHitCounter);
	return ctr;
     }
}
