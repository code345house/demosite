package tank.game;

import java.awt.Color;


public class ObjBasicMoveableClass extends ObjBasicClass 
{

    protected ObjBasicCollisionDetector objBasicCollisionDetector;
	
	
    public ObjBasicMoveableClass(double x,double y,int idNumber,Color color,
		        ObjBasicCollisionDetector objBasicCollisionDetector)
    {
	super(x,y,idNumber,color);		
	this.objBasicCollisionDetector =  objBasicCollisionDetector;
	
		
    } 
	
	

}
