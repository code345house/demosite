package tank.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

/*
* ------------------------------------------------------------------------
* 
*  author  : Harri Isom�ki
*  
*  Class   : ObjBasicExplosionThreadTriangle
*  
*  -----------------------------------------------------------------------
*     
* 
*                          -----------------------------
*                          !  objBasicExplosionThread  !
*                          -----------------------------
*                                        !
*                                        !
*               ---------------------------------------
*               !                                     !  
*      -----------------------------------            !
*      !  ObjBasicExplosionThreadSquare  !   	      !  
*      -----------------------------------            !
*                                                     !
*                                                     !  
*                                  -------------------------------------
*                                  !  ObjBasicExplosionThreadTriangle  !
*                                  -------------------------------------
*
*
*
*
*
*  When House object is hit by ammunition from the tank gun, ObjBasicClass
*  generates explosion by starting three objects.
*
*           ObjBasicExplosionFigure
*           ObjBasicExplosionThreadSquare
*           ObjBasicExplosionThreadTriangle
*
*  These objects create illusion of an explosion.
*
*
*  ObjBasicExplosionThreadTriangle is sub class according to figure above.
*
*  Figure of square is generated with help of polar coordinates and 
*  polygons
* 
* ------------------------------------------------------------------------
*/



public class ObjBasicExplosionThreadTriangle1 extends ObjBasicExplosionThread
{

public  ObjBasicExplosionThreadTriangle1(Color color){	super(color);}
	
	
    public void paint(Graphics g)
    {
		
        if(isExplosionActive)
        {			
            ye1 = line * Math.sin(Math.PI/8.57 + angle);    
            xe1 = line * Math.cos(Math.PI/8.57 + angle);
	 
            ye2 = line * Math.sin(Math.PI-Math.PI/8.57 + angle);
            xe2 = line * Math.cos(Math.PI-Math.PI/8.57 + angle);
		
            ye3 = line * Math.sin(Math.PI + Math.PI/8.57 + angle);
            xe3 = line * Math.cos(Math.PI + Math.PI/8.57 + angle);		
		  
		
            ye1 = ye1 + y;
            ye2 = ye2 + y;
            ye3 = ye3 + y;
    		
            xe1 = xe1 + x;
            xe2 = xe2 + x;
            xe3 = xe3 + x;		    
		    
		
            ye1 = (int)Math.round(ye1) ;
            ye2 = (int)Math.round(ye2) ;
            ye3 = (int)Math.round(ye3) ;
		    
            xe1 = (int)Math.round(xe1) ;
            xe2 = (int)Math.round(xe2) ;
            xe3 = (int)Math.round(xe3) ;
		    
		
            Polygon explosionTriangle = new Polygon();	
			
            explosionTriangle.addPoint((int)xe1,(int)ye1);
            explosionTriangle.addPoint((int)xe2,(int)ye2);
            explosionTriangle.addPoint((int)xe3,(int)ye3);		    		
		
            g.setColor(color);
            g.fillPolygon(explosionTriangle);
     }

   }

}
