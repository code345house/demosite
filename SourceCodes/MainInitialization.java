package tank.game;

import java.awt.Color;

/*
 * ------------------------------------------------------------------------
 * 
 *  author  : Harri Isom�ki
 *  
 *  Class   : MainInitialization
 *  
 * ------------------------------------------------------------------------
 * 
 *  Tank, ammunition,house and collisionDetector objects are initialized in 
 *  this class.
 *
 *  Tank, ammunition and house objects are attached to objectRegister.
 *  ObjectRegister is called at regular intervals by paint method.
 *  
 *  ObjectRegister is used also by collisionDetector, when it compares
 *  coordinates between ammunition / tank and house objects to determine
 *  existence of collision.
 *
 *--------------------------------------------------------------------------
 */




public class MainInitialization 
{
    protected int ammunitionCounter = 0 ;	
    protected int ammunitionNumber = 30;	
    protected Thread t1;	
    protected MainClass mainClass;	
    protected ObjBasicClass[] objectRegister = new ObjBasicClass[100];	
    protected ObjectAmmunition[] ammunitionStorage = new  ObjectAmmunition[ammunitionNumber];	
    protected ObjBasicCollisionDetector objBasicCollisionDetector;	
    protected ObjectTank objectTank ;
	
	
	
    public MainInitialization(MainClass mainClass)
    {
	this.mainClass =mainClass;
		
	objBasicCollisionDetector = new ObjBasicCollisionDetector(objectRegister,mainClass);		
	objectTank = new ObjectTank(20,440,1,Color.yellow,objBasicCollisionDetector);				
	t1 = new Thread();
				
	objectRegister[0] = objectTank;
		
	objectRegister[1] = new ObjectHouse(666,430,2,Color.blue);		
	objectRegister[2] = new ObjectHouse(777,430,3,Color.red);		
	objectRegister[3] = new ObjectHouse(744,244,4,Color.red);			
		
        objectRegister[4] = new ObjectHouse(666,144,5,Color.blue);		
	objectRegister[5] = new ObjectHouse(999,222,6,Color.green);		
	objectRegister[6] = new ObjectHouse(455,274,7,Color.magenta);		
		
        objectRegister[7] = new ObjectHouse(533,288,8,Color.blue);		
	objectRegister[8] = new ObjectHouse(440,430,9,Color.green);		
	objectRegister[9] = new ObjectHouse(755,288,9,Color.green);	
		
		
		
	int idNumber = 30;
	for(int i=0; i < 30 ;i++)
	{              
	    idNumber = idNumber + 1;
	    ammunitionStorage[i] = new ObjectAmmunition(11, 11,idNumber, null,
			                objBasicCollisionDetector, objectTank);
	}
		
		
		
	int y = 0;
	for(int i=9; i < (9+ ammunitionNumber);i++)
	{ 
	   objectRegister[i]= ammunitionStorage[y];
	   y =y +1;
        }
	    
	    
	    
	try 
	{
		t1.sleep(20);			
	} 
	catch (InterruptedException e)
	{
			e.printStackTrace();		
	}	 
    }
	
    public void increaseAmmunitionCounter(){	ammunitionCounter ++;}
    public int getAmmunitionCounter(){ return ammunitionCounter;}	
    public int getAmmunitonNumber(){return ammunitionNumber;}
	
	
}
