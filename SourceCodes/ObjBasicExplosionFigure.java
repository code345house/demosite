package tank.game;

import java.awt.Color;
import java.awt.Graphics;

/*
* ------------------------------------------------------------------------
* 
*  author  : Harri Isom�ki
*  
*  Class   : ObjBasicExplosionFigure
*  
*-------------------------------------------------------------------------
*     
*
*
* When House object is hit by ammunition from the tank gun, ObjBasicClass
* generates explosion by starting three objects.
*
*           ObjBasicExplosionFigure
*           ObjBasicExplosionThreadSquare
*           ObjBasicExplosionThreadTriangle
*
* These objects create illusion of an explosion.
*
*
* ObjBasicExplosionFigure starts a thread , which is implemented by
* extending Thread class
*
* Small yellow and red squares are generated. Some ones begin to move to 
* south, other ones move to north, west and east. At first their size
* increase. Then size begins to decrease.
* 
* -------------------------------------------------------------------------
*/




public class ObjBasicExplosionFigure extends Thread
{
    private boolean isExplosionActive =false;    
    private int x,y,width=4,heigth=4,stepCounter;    
    private Color color;
    
    
    public  ObjBasicExplosionFigure(Color color)
    {
        this.color = color;	
    }
	
	
    public void coordinateTransfer(double x,double y)
    {
        this.x =(int) x;
        this.y =(int) y;
    }
    
    

 
    public void run()
    {
        isExplosionActive = true;
    	
    	
        while(stepCounter < 333)
        {
           stepCounter++ ;
          	
           if(stepCounter < 5)
           {
              width++;
              heigth++;
           }
           else
           {
              width--;
             heigth--;
           }
        	
        	
        	
        try
        {
	    Thread.sleep(33);
        }
        catch (InterruptedException e)
        {				
	     e.printStackTrace();
        }       	
       }        
        
        isExplosionActive = false;
}
    
    
    
    
    
    
    public void paint(Graphics g)
    {
       if(isExplosionActive)
       { 
	   g.setColor(color.yellow);
	    	
	   g.fillRect(x,y, width, heigth); 	    	
	    	
	   g.fillRect(x + 2* width ,y, width, heigth); 
	   g.fillRect(x - 2* width ,y, width, heigth);	    	
	   g.fillRect(x  ,y + 2* width, width, heigth); 
           g.fillRect(x  ,y - 2* width, width, heigth); 
	    	
	    	
	   g.setColor(color.red);
	   	
	   g.fillRect(x + 4* width ,y, width, heigth); 
	   g.fillRect(x - 4* width ,y, width, heigth);	    	
	   g.fillRect(x  ,y + 4* width, width, heigth); 
	   g.fillRect(x  ,y - 4* width, width, heigth); 
	    		    	
	    	
  	   g.setColor(color.yellow);
	    	
    	   g.fillRect(x + 6* width ,y, width, heigth); 
	   g.fillRect(x - 6* width ,y, width, heigth);	    	
	   g.fillRect(x  ,y + 6* width, width, heigth); 
	   g.fillRect(x  ,y - 6* width, width, heigth); 
	    	
	    	
	   g.setColor(color.red);	 
	    	
	   g.fillRect(x + 2* width ,y + 2* width, width, heigth); 	    	
	   g.fillRect(x + 2* width ,y - 2* width, width, heigth); 	    	
	   g.fillRect(x - 2* width ,y - 2* width, width, heigth); 	    	
	   g.fillRect(x - 2* width ,y + 2* width, width, heigth); 
	    	
	    	
	   g.setColor(color.yellow);
	    	
           g.fillRect(x + 4* width ,y + 4* width, width, heigth); 	    	
           g.fillRect(x + 4* width ,y - 4* width, width, heigth); 	    	
           g.fillRect(x - 4* width ,y - 4* width, width, heigth); 	    	
   	   g.fillRect(x - 4* width ,y + 4* width, width, heigth); 
         }
      }
}
