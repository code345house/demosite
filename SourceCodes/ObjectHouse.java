package tank.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

/*
* ------------------------------------------------------------------------
* 
*  author  : Harri Isom�ki
*  
*  Class   : ObjectHouse
*  
* ------------------------------------------------------------------------
*
* House is painted  with help of polygons. At first roof is painted 
* and finally basis.
*
*--------------------------------------------------------------------------
*/


public class ObjectHouse extends ObjBasicStaticClass
{
   private Polygon roof,basis;
	
	
   public ObjectHouse(double x, double y, int idNumber, Color color)
   {
       super(x, y, idNumber, color);
   }

	
	
   public void paint(Graphics g)
   {
      if(!hasCollisionHappened)
      {			
	   roof = new Polygon();
		
	   roof.addPoint((int)x + 20,(int)y);
	   roof.addPoint((int)x + 25,(int)y + 12);
	   roof.addPoint((int)x + 20,(int)y + 24);
	   roof.addPoint((int)x + 42,(int)y + 24);
	   roof.addPoint((int)x + 47,(int)y + 12);
	   roof.addPoint((int)x + 42,(int)y );
		
	   g.setColor(Color.black);
	   g.fillPolygon(roof);
		
		
	   basis = new Polygon();
		
	   basis.addPoint((int)x + 10,(int)y + 5);
	   basis.addPoint((int)x + 20,(int)y );
	   basis.addPoint((int)x + 25,(int)y + 12);
	   basis.addPoint((int)x + 20,(int)y + 24);
	   basis.addPoint((int)x + 10,(int)y + 16);
		
		
	   g.setColor(color);
	   g.fillPolygon(basis);
		
     }	
   }
	

}
