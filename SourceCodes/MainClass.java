package tank.game;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.net.MalformedURLException;
import java.net.URL;

/*
 * ------------------------------------------------------------------------
* 
*  author  : Harri Isom�ki
*  
*  Class   : MainClass
*  
* ------------------------------------------------------------------------
 * 
 *  
 *  When program is started method CheckPassWord is executed.
 *
 *  When user press " START GAME " button, GameLoopThread is started.
 *  Tank object is started. Tank begins to move forward on the screen. 
 *
 *  Paint method with double buffering will be updated with regular intervals.
 *
 *  MousePressed and keyPressed events are executed in this class.
 *  
 *-------------------------------------------------------------------------
 */



public class MainClass extends Applet implements Runnable,MouseListener,
                                                 MouseMotionListener,KeyListener
{
	
    private MainModalMenu passwordMenu;
    private MainClass mainClass;
    private MainScreenPanel mainScreenPanel;
    private MainInitialization mainInitialization;	
    private Rectangle startGameBox,startNewGameBox;
	
    private Thread gameLoopThread;
    private Thread t2;
		
    private String username,password;
    private String message= "";
    private String mcAmmunitionNbr ="";
    private String mcHits = "";	
	
    private boolean pwSuccess = true;
    private boolean stopGameButtonPressed = false;
    private boolean startGameButtonPressed = false;
    private boolean isGamePressed =false;	
	
    private double turretAngle =0,tankAngle = 0.13;
    private Image dbImage;
    private Graphics backGround;
	
    private int spaceCntr =0;
	
	
	

	
    public void init()
    {
	this.setSize(1200,700);
	    
	this.addMouseListener(this);
	this.addMouseMotionListener(this);
	this.addKeyListener(this);
	    	    
	    
	if(!checkPassWord())
        {
	    message= " login not valid ";
	}
	else
	{
	   message= " login valid";
        }
	    
	    
	    
	    
	if(passwordMenu.getButtonStatus())
	{ 
	   initializeObjects();
	}
	else                                            
	{
	   try 
           {
		this.getAppletContext().showDocument(new URL(this.getCodeBase() 
			                              +"start.html"),"_top");
	   }
	   catch (MalformedURLException e) {	e.printStackTrace();		}
	}
	  
     }
	
	
	

    public void initializeObjects()
    {
	mainInitialization = new MainInitialization(this);
		
	startGameBox = new Rectangle(30,600,160,50);	    
	startNewGameBox = new Rectangle(300,600,160,50);
	 mainScreenPanel = new MainScreenPanel(this,mainInitialization,startGameBox,
	    		              startNewGameBox,isGamePressed );
	t2 = new Thread();
	gameLoopThread = new Thread(this);
		
	spaceCntr = 0;
     }

	
	
    public void update(Graphics g)
    {
	dbImage = createImage(this.getWidth(),this.getHeight());		
	backGround = dbImage.getGraphics();		
	mainScreenPanel.paint(backGround);		 
	g.drawImage(dbImage, 0, 0, this);
    }
	
	
    public void paint(Graphics g)
    {		
	update(g);
    }
	
	
	
	
    @Override
    public void run() 
    {		
       Thread thisThread = Thread.currentThread();                
       mainInitialization.objectTank.start();  
        
       while(gameLoopThread == thisThread)
       {
	  repaint(); 
				
	  try 
	  {
	     Thread.sleep(5);
	  }
           catch (InterruptedException e) 
	  {			
              e.getStackTrace();
	  }		
	}

     }

	
	
	
	
	
    @Override
    public void mousePressed(MouseEvent e)
    {
       int mx = e.getX();
       int my = e.getY();		
		
		
	if( mx > startGameBox.x  && mx < ( startGameBox.x + startGameBox.width) && 
	    my > startGameBox.y  && my < ( startGameBox.y + startGameBox.height) && 
	    startGameButtonPressed == false	)			
	{
	    gameLoopThread.start();
		
	    mainScreenPanel.setGamePressedTrue();			
     	    stopGameButtonPressed = false ;
     	    startGameButtonPressed =true;
	}
		
		
		
		
		
	if( mx > startNewGameBox.x  && mx < ( startNewGameBox.x + startNewGameBox.width)
	    &&  my > startNewGameBox.y  && my < ( startNewGameBox.y + startNewGameBox.height)
	    &&  stopGameButtonPressed == false)
	{		
	    mainInitialization.objectTank.tankThread.interrupt();
			
	    mcAmmunitionNbr =Integer.toString(mainInitialization.getAmmunitionCounter());				
	    mcHits = mainInitialization.objBasicCollisionDetector.getHits(); 
			
	    mainScreenPanel.setGamePressedFalse();	
     	    stopGameButtonPressed = true;
     	    startGameButtonPressed =false;
     	    
     	    Toolkit.getDefaultToolkit().beep();		    
            startInitialization();
	}		
     }
	
	
	
    public String aCntr(){return mcAmmunitionNbr;}	
    public String hCntr(){return mcHits;}
	
	
	
    public void startInitialization()
    {
	try 
	{
	   t2.sleep(30);			 
	} 
	catch (InterruptedException e1) {e1.printStackTrace(); }
		
	initializeObjects();
    }
	
	
	

    @Override
    public void keyPressed(KeyEvent e) 
    {		
	if(tankAngle > 2*Math.PI){ tankAngle = tankAngle - 2*Math.PI;}		
	int  key = e.getKeyCode();
		
		
	if(key == KeyEvent.VK_RIGHT)
	{
	    tankAngle = tankAngle + 0.0086;			
	    mainInitialization.objectTank.rotateTank(tankAngle);		
	}
		
		
	
	if(key == KeyEvent.VK_LEFT)
	{
	    tankAngle = tankAngle - 0.0086;			
	    mainInitialization.objectTank.rotateTank(tankAngle);			
	}
		
		
		
		
	if(key == KeyEvent.VK_Q)
	{
	    turretAngle = turretAngle + 0.011;			
	    mainInitialization.objectTank.rotateTurret(turretAngle);			
	}		
		
	
	if(key == KeyEvent.VK_W)
	{
	    turretAngle = turretAngle - 0.011;			
	    mainInitialization.objectTank.rotateTurret(turretAngle);		
	}


		
		
	if(key == KeyEvent.VK_UP)
	{
	    mainInitialization.objectTank.stopTank();
	}

		
	if(key == KeyEvent.VK_DOWN)
	{
	    mainInitialization.objectTank.startTank();	
	}
		
		
		
		
  	if(key == KeyEvent.VK_SPACE)
	{
  	    spaceCntr = spaceCntr + 1;
  			
  			
            if(mainScreenPanel.getGamePressedStatus() == true 
               && mainInitialization.getAmmunitionCounter() 
               < mainInitialization.getAmmunitonNumber() )
            	
	    {
				
		mainInitialization.ammunitionStorage[mainInitialization
		    	                   .getAmmunitionCounter()].start();
		    	
		mainInitialization.increaseAmmunitionCounter();	 			    
 		AudioClip voice = this.getAudioClip(getDocumentBase(),
 			    		          "gunShotSound.wav");
 		voice.play();
 			    
 			    
 	        if( spaceCntr < mainInitialization.getAmmunitonNumber()+1 )
 		{	
 		     mcAmmunitionNbr =Integer.toString(mainInitialization
 			    		         .getAmmunitionCounter());				
 				    
 		     mcHits = mainInitialization.objBasicCollisionDetector.getHits();  
 		}
 			    
				
	    }
	    else
	    {

			
		if(stopGameButtonPressed == false  && mainInitialization
		      .getAmmunitionCounter()   == mainInitialization.getAmmunitonNumber())
		{
					Toolkit.getDefaultToolkit().beep();
		}				 
			
		stopGameButtonPressed = true;
	     	startGameButtonPressed =false;			
				
		startInitialization();
		}
	    }
	}
	
	
	
	
	public void generateExplosionSound()
	{
	    AudioClip voice2 = this.getAudioClip(getDocumentBase(),"explosionSound.wav");
	    voice2.play();	
		
	}
	
	


	public String getNumberOfAmmunition()
	{
	    String aNumber =Integer.toString( mainInitialization.getAmmunitionCounter());
	    return aNumber;
	}
	
	
	
	private boolean checkPassWord()
	{
	   boolean isLoginValid=false;
		
	   passwordMenu = new MainModalMenu(new Frame(""),pwSuccess);	
	   this.requestFocus();		

		
		
	   if(!passwordMenu.getButtonStatus())
	   {
	       passwordMenu.dispose();
	   }
	   else
	   {
		username = passwordMenu.username.getText();				
		char[] tempA = passwordMenu.password.getPassword();			
		password =new String(tempA);					
			
		if (username.equals("qw") && password.equals("zx"))
		{
		   isLoginValid = true;
		}
		else
		{
		    pwSuccess = false;
		   init();
		}		
	  }
		
		
	     passwordMenu.dispose();
	     return isLoginValid;
	}

	
    public void keyReleased(KeyEvent arg0) {}	
    public void keyTyped(KeyEvent arg0) {}
    public void mouseDragged(MouseEvent arg0) {}	
    public void mouseMoved(MouseEvent arg0) {}	
    public void mouseClicked(MouseEvent arg0) {}
    public void mouseEntered(MouseEvent arg0) {}
    public void mouseExited(MouseEvent arg0) {}
    public void mouseReleased(MouseEvent arg0) {}
	
	
}
