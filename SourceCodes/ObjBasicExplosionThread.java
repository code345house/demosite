package tank.game;

import java.awt.Color;

/*
* ------------------------------------------------------------------------
* 
*  author  : Harri Isom�ki
*  
*  Class   : ObjBasicExplosionThread
*  
* ------------------------------------------------------------------------
*     
* 
*                          -----------------------------
*                          !  objBasicExplosionThread  !
*                          -----------------------------
*                                        !
*                                        !
*               ---------------------------------------
*               !                                     !  
*      -----------------------------------            !
*      !  ObjBasicExplosionThreadSquare  !   	      !  
*      -----------------------------------            !
*                                                     !
*                                                     !  
*                                  -------------------------------------
*                                  !  ObjBasicExplosionThreadTriangle  !
*                                  -------------------------------------
*
*
*
*
*
*  When House object is hit by ammunition from the tank gun, ObjBasicClass
*  generates explosion by starting three objects.
*
*           ObjBasicExplosionFigure
*           ObjBasicExplosionThreadSquare
*           ObjBasicExplosionThreadTriangle
*
*  These objects create illusion of an explosion.
*
*
*  ObjBasicExplosionThread is super class according to figure above.It has 
*  methods and variables , which are common to ObjBasicExplosionThreadSquare
*  and ObjBasicExplosionThreadTriangle classes .
*
*  ObjBasicExplosionThread starts a thread , which is implemented with 
*  runnable interface.
*
*  Figure ( square or triangle ) is generated with help of polar
*  coordinates. Figure begins to move direction determined by random
*  generator. At first its size  increases,but then size begins to decrease.
*
*  This figure is built with polygons in ObjBasicExplosionThreadSquare or
*  ObjBasicExplosionThreadTriangle class. 
* 
* ------------------------------------------------------------------------
*/





public class ObjBasicExplosionThread implements Runnable
{
   protected boolean isExplosionActive =false;
   protected double x,y,xe1,xe2,xe3,xe4,ye1,ye2,ye3,ye4,incX,incY,line=4,angle=0;	   
   protected Color color;	
   protected int step = 0;
   protected Thread explosionThread;
	
	
   public  ObjBasicExplosionThread (Color color)
   {
       this.color = color;	
		
		
       double nbr = Math.random();
       nbr = 4 * nbr;
       int direction = (int) nbr;		
		
      switch(direction)
      {
          case 0 : incX = 0;incY = 1; break;		
          case 1 : incX = 0;incY = -1; break;		
          case 2 : incX = 1; incY = 0; break;		
          case 3 : incX = -1;incY = 0 ; break;
      }
		
    }  
	


    public void coordinateTransfer(double x,double y)
    {
       this.x =(int) x;
       this.y =(int) y;
    }
	
	
	 
    public void start() 
    {
       explosionThread = new Thread(this);
       explosionThread.start();		
    }
	
	
	
	

     @Override
     public void run() 
     {
        isExplosionActive = true;		

        while(step < 150)
        {
			
           step = step +1;				
	   angle =angle + 3.142/33;			
		
	   if(step < 66)
	   {
	       line = line + (0.04 *line );
	   }
	   else
	   {
	        line = line - (0.04 *line );
	   }

				
	   x = x + incX;
	   y = y + incY;
			
		 
	   try
	   {
	     Thread.sleep(58);
	   }
	   catch (InterruptedException e)
	   {
	      e.printStackTrace();
	  }

        }
        isExplosionActive = false;
     }

}
