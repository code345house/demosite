package tank.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JPanel;

/*
* ------------------------------------------------------------------------
* 
*  author  : Harri Isom�ki
*  
*  Class   : MainScreenPanel
*  
* ------------------------------------------------------------------------
* 
*  Tank, ammunition,house and explosion objects are painted in this class.
*
*  Method for painting gradient background is executed.
*
*  Menus for screen are painted.
*  
*------------------------------------------------------------------------
*/


public class MainScreenPanel extends JPanel
{
   private MainClass mainClass;
   private MainInitialization mainInitialization;		
   private Rectangle startGameBox,startNewGameBox;	
   private Font font;
   private int objNumber=39;
   private boolean isGamePressed =false;
   private boolean isStopGamePressed = false;
   private boolean isInitScreenActive = false;
	
	
	
   public MainScreenPanel( MainClass mainClass,MainInitialization mainInitialization,
			                Rectangle startGameBox,Rectangle startNewGameBox,
			                boolean isGamePressed)			                
   {
       this.mainClass =  mainClass;		
       this.mainInitialization = mainInitialization;	
		
       this.startGameBox =  startGameBox;		
       this.startNewGameBox = startNewGameBox;	
       this.isGamePressed =  isGamePressed; 
   }
	

   public  void setGamePressedTrue() {isGamePressed = true;}  	
   public  void setGamePressedFalse() {isGamePressed = false;}	
   public boolean getGamePressedStatus() { return isGamePressed ;}
	
	


   public void paint(Graphics g)
   {	       
		
       Color gradientColor = new Color(168,161,20); 	 	
       drawGradientBackground(this,g,gradientColor,Color.white); 		
		
		
		
       for(int i=0;i < objNumber;i++)
       {
	  mainInitialization.objectRegister[i].paint(g);
	    
	  mainInitialization.objectRegister[i].objBasicExplosionFigure.paint(g);
	    
	  mainInitialization.objectRegister[i].objBasicExplosionThreadSquare1.paint(g);
	  mainInitialization.objectRegister[i].objBasicExplosionThreadSquare2.paint(g);
	    
	  mainInitialization.objectRegister[i].objBasicExplosionThreadTriangle1.paint(g);
	  mainInitialization.objectRegister[i].objBasicExplosionThreadTriangle2.paint(g);
       }
	 
	 
	 
	 
	 
	 
//------------- MAIN MENU------------------------------------------------------------	 
 	 
	 

	 if(isGamePressed == false)           // stop pressewd--GAME OVER
	 {
		
	     g.setColor(Color.black);
	     g.fillRect(0,0,1200,600);
	     
	     g.setColor(Color.yellow);	
	     
	     font = new Font("Helvetica",Font.BOLD ,55);    		 
	     g.setFont(font);
	     g.drawString(" TANK GAME ",400,90);      
	     
	     
             g.drawLine(0,130,1200,130);	
         
         
             font = new Font("SansSerif",Font.PLAIN,12);  		 
	     g.setFont(font);        

	     
             g.drawString(" You can start tank game by pressing  \"start game\"  button at"
        		     + " lower left corner",10,170); 	     
             g.drawString(" Then tank starts to move forward ",10,190);  	     
             g.drawString(" You can stop the tank by pressing ARROW UP  key",10,230);          
             g.drawString(" You can start the tank by pressing ARROW DOWN   key",10,250); 
             g.drawString(" You can change the direction to left  by pressing ARROW LEFT  "
        		     + "key",10,270);
             g.drawString(" You can change the direction to right  by pressing  ARROW RIGHT"
        		+ " key",10,290);
             g.drawString(" You can rotate the turret to left by pressing  W key",10,310);        
             g.drawString(" You can rotate the turret to right by pressing Q key",10,330); 
             g.drawString(" You can fire the gun by pressing SPACE ",10,350);	     
             g.drawString(" The game will be stopped when all ammunition ( 30 ) is used or"
        		     + " when player stops the game",10,390       ); 
                
             g.drawLine(0,430,1200,430);	        
        
        
             font = new Font("Helvetica",Font.BOLD ,33);    		 
	     g.setFont(font);
        
        
             g.drawString("THE FINAL RESULTS",40,480); 
        
             g.drawString("HITS :", 40, 540);  
             g.drawString(mainClass.hCntr(), 200, 540);
        
        
             g.drawString("AMMUNITION USED :", 585, 540);        
             g.drawString(mainClass.aCntr(), 995, 540);
        
        
             g.drawLine(0,585,1200,585);
        
        
        
	     
	     
 //------------------------ START MENU--------------------------------------------	     
	 		     
	 
	     g.setColor(Color.black);	 
	     g.fillRect(0,600,1200,600);
	 
	     g.setColor(Color.yellow);	 
	     g.fill3DRect(startGameBox.x,startGameBox.y,startGameBox.width,
	    		      startGameBox.height,true);	 
	    
	     font = new Font("SansSerif",Font.BOLD ,18);    		 
	     g.setFont(font);
	     g.setColor(Color.red);
	     g.drawString("START GAME",startGameBox.x +15, startGameBox.y +30);	 
	 }
	 
	 
	 
	 
//------------- STOP MENU ---------------------------------------------------------	 
	 	 
	 
	 if(isGamePressed)           
	 {
		 
	     g.setColor(Color.black);	 
	     g.fillRect(0,0,1200,80);
		 
	     g.setColor(Color.yellow);   
	     g.drawString(" stop  tank ---- ARROW UP ",10,33);
	     g.drawString(" start  tank ---  ARROW DOWN ",10,55);	     
	     g.drawString(" change direction to left   ----  ARROW LEFT ",270,33);
	     g.drawString(" change direction to right ----  ARROW RIGHT ",270,55);	     
	     g.drawString(" rotate turret to left   -----   press W ",600,33);
	     g.drawString(" rotate turret to right -----  press Q ",600,55);	     
	     g.drawString(" fire gun -----  press SPACE ",880,33);		 
		 
	     font = new Font("SansSerif",Font.BOLD ,18);    		 
	     g.setFont(font);
		 
	     g.setColor(Color.black); 	   
	     g.fillRect(0,560,1200,560);
	 
             g.setColor(Color.yellow);	 
	     g.fill3DRect(startNewGameBox.x,startNewGameBox.y,startNewGameBox.width,
	    		      startNewGameBox.height,true);
	 
	     g.setColor(Color.red);
	     g.drawString("STOP GAME",startNewGameBox.x +15, startNewGameBox.y + 30);
	 }
	 
	 
	 
//------------- HIT / AMMUNITION BOXES-- lower part of screen--------------------	 
	 
	 
       g.setColor(Color.yellow);           
       g.fill3DRect(580,600,250,50,true);  
     
     
       font = new Font("SansSerif",Font.BOLD ,18);    		 
       g.setFont(font);
       g.setColor(Color.red);
     
       g.setColor(Color.red); 
       g.drawString("HITS :", 660, 630);    
     
       font = new Font("SansSerif",Font.BOLD ,18);    		 
       g.setFont(font);
       g.drawString( mainInitialization.objBasicCollisionDetector.getHits(), 750, 630);  
          
     
       g.setColor(Color.yellow);     
       g.fill3DRect(850,600,250,50,true);  
          
       font = new Font("SansSerif",Font.BOLD ,18);    		 
       g.setFont(font);
       g.setColor(Color.red);
       g.drawString("Ammunition used :", 860, 630);      
     
       font = new Font("SansSerif",Font.BOLD ,18);    		 
       g.setFont(font);
       g.drawString(mainClass.getNumberOfAmmunition(), 1050, 630);     
     }


	
	
     private void drawGradientBackground(MainScreenPanel mainScreenPanel,
			Graphics g, Color start, Color end) 
     {
	float gHeigth =500f;		
	float gWidth = 1200f;
		
        float gRed1 = start.getRed()/255.0f;
        float gGreen1 = start.getGreen()/255.0f;
        float gBlue1 = start.getBlue()/255.0f;
     
        float gRed2 = end.getRed()/255.0f;
        float gGreen2 = end.getGreen()/255.0f;
        float gBlue2 = end.getBlue()/255.0f;
     
        float dr = (gRed2-gRed1)/gHeigth;
        float dg = (gGreen2-gGreen1)/gHeigth;
        float db = (gBlue2-gBlue1)/gHeigth;    
     
   
     
         for(int y=0;y<gHeigth;y++)
         {
    	    g.setColor(new Color(gRed1,gGreen1,gBlue1));    	 
    	    g.drawLine(0, y,(int) (gWidth-1),y);   
    	 
    	    gRed1 += dr;
    	    gGreen1 += dg;
    	    gBlue1 += db;
         }	 
	
     }
}
