package tank.game;

import java.awt.Color;
import java.awt.Graphics;

/*
 * ------------------------------------------------------------------------
 * 
 *  author  : Harri Isom�ki
 *  
 *  Class   :  ObjBasicClass
 *  
 *-------------------------------------------------------------------------
 *     
 *     
 * 
 *                            ------------------
 *                            !  ObjBasicClass !
 *                            ------------------
 *                                    !
 *                                    !
 *               ---------------------------------------------
 *               !                                           !  
 *      -----------------------             -------------------------
 *      ! ObjBasicStaticClass !             ! ObjBasicMoveableClass !   
 *      -----------------------             -------------------------
 *               !                                    !
 *               !                                    !
 *               !                                    !
 *               !                         ---------------------                                                                      
 *               !                         !                   !
 *       ---------------       --------------------     --------------
 *       ! ObjectHouse !       ! ObjectAmmunition !     ! ObjectTank !               
 *       ---------------       --------------------     --------------
 *
 *
 *
 *
 *
 *  ObjBasicClass is super class according to figure above. It has methods 
 *  and variables, which are common to House, Ammunition and Tank classes.
 *
 *  When House object is hit by ammunition from the tank gun, ObjBasicClass
 *  generates explosion by starting three objects.
 *
 *           ObjBasicExplosionThread
 *           ObjBasicExplosionThreadSquare
 *           ObjBasicExplosionThreadTriangle
 *
 *  These objects create illusion of an explosion.
 *
 *
 *  Methods getObjectCoordinate_x() and getObjectCoordinate_y() are used by
 *  ObjBasicCollisionDetector to determine existence of collision  between
 *  House and Ammunition objects.
 *  
 *-----------------------------------------------------------------------------
 */


public class ObjBasicClass 
{

    protected double x,y,ObjX,ObjY;
    protected int idNumber;
    protected Color color;
	
    protected boolean hasCollisionHappened = false;
	
    protected ObjBasicExplosionFigure objBasicExplosionFigure;
    protected ObjBasicExplosionThreadSquare1  objBasicExplosionThreadSquare1;
    protected ObjBasicExplosionThreadSquare2  objBasicExplosionThreadSquare2;
    protected ObjBasicExplosionThreadTriangle1 objBasicExplosionThreadTriangle1;
    protected ObjBasicExplosionThreadTriangle2 objBasicExplosionThreadTriangle2;
	
	
    public ObjBasicClass(double x, double y, int idNumber, Color color)
    {
	this.x = x;	
	this.y = y;	
	this.idNumber = idNumber;
	this.color =  color;
		
	objBasicExplosionFigure = new ObjBasicExplosionFigure(color);
		
	objBasicExplosionThreadSquare1 = new  ObjBasicExplosionThreadSquare1(color);
	objBasicExplosionThreadSquare2 = new  ObjBasicExplosionThreadSquare2(color);
		
	objBasicExplosionThreadTriangle1 = new ObjBasicExplosionThreadTriangle1(color);
	objBasicExplosionThreadTriangle2 = new ObjBasicExplosionThreadTriangle2(color);
     }



     public double getObjectCoordinate_x()
     {
	 ObjX = x + 22;
	 return ObjX;
     }
	
	

     public double getObjectCoordinate_y()
     {
         ObjY = y + 22;
	 return ObjY;
     }
	
	
	
     public void generateExplosion()
     {
			
	 hasCollisionHappened = true;
		
	 objBasicExplosionFigure.coordinateTransfer(x, y);
	 objBasicExplosionFigure.start();		
				
	 objBasicExplosionThreadSquare1.coordinateTransfer(x, y);
	 objBasicExplosionThreadSquare1.start();
		
	 objBasicExplosionThreadSquare2.coordinateTransfer(x, y);
	 objBasicExplosionThreadSquare2.start();
						
	 objBasicExplosionThreadTriangle1.coordinateTransfer(x, y);
	 objBasicExplosionThreadTriangle1.start();
		
	 objBasicExplosionThreadTriangle2.coordinateTransfer(x, y);
	 objBasicExplosionThreadTriangle2.start();
      }
		


     public void paint(Graphics g){}
	
	
}
