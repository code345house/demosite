package tank.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;


/*
* ------------------------------------------------------------------------
* 
*  author  : Harri Isom�ki
*  
*  Class   : ObjBasicExplosionThreadSquare
*  
*  -----------------------------------------------------------------------
*     
* 
*                          -----------------------------
*                          !  objBasicExplosionThread  !
*                          -----------------------------
*                                        !
*                                        !
*               ---------------------------------------
*               !                                     !  
*      -----------------------------------            !
*      !  ObjBasicExplosionThreadSquare  !   	      !  
*      -----------------------------------            !
*                                                     !
*                                                     !  
*                                  -------------------------------------
*                                  !  ObjBasicExplosionThreadTriangle  !
*                                  -------------------------------------
*
*
*
*
*
*  When House object is hit by ammunition from the tank gun, ObjBasicClass
*  generates explosion by starting three objects.
*
*           ObjBasicExplosionFigure
*           ObjBasicExplosionThreadSquare
*           ObjBasicExplosionThreadTriangle
*
*  These objects create illusion of an explosion.
*
*
*  ObjBasicExplosionThreadSquare is sub class according to figure above.
*
*  Figure of square is generated with help of polar coordinates and 
*  polygons
* 
* ------------------------------------------------------------------------
*/



public class ObjBasicExplosionThreadSquare1 extends ObjBasicExplosionThread
{

    public  ObjBasicExplosionThreadSquare1(Color color)	{super(color);}
	
	
    public void paint(Graphics g)	
    {		
       if(isExplosionActive)
       {			
		
           ye1 = line * Math.sin(Math.PI/8.57 + angle);    
           xe1 = line * Math.cos(Math.PI/8.57 + angle);
	
           ye2 = line * Math.sin(Math.PI-Math.PI/8.57 + angle);
           xe2 = line * Math.cos(Math.PI-Math.PI/8.57 + angle);
		
           ye3 = line * Math.sin(Math.PI + Math.PI/8.57 + angle);
           xe3 = line * Math.cos(Math.PI + Math.PI/8.57 + angle);
		
           ye4 = line * Math.sin(2*Math.PI-Math.PI/8.57 + angle);
           xe4 = line * Math.cos(2* Math.PI-Math.PI/8.57 + angle);
		    
		
           ye1 = ye1 + y;
           ye2 = ye2 + y;
           ye3 = ye3 + y;
           ye4 = ye4 + y;
		
		
           xe1 = xe1 + x;
           xe2 = xe2 + x;
           xe3 = xe3 + x;
           xe4 = xe4 + x;
		    
	
           ye1 = (int)Math.round(ye1) ;
           ye2 = (int)Math.round(ye2) ;
           ye3 = (int)Math.round(ye3) ;
           ye4 = (int)Math.round(ye4) ;
	
           xe1 = (int)Math.round(xe1) ;
           xe2 = (int)Math.round(xe2) ;
           xe3 = (int)Math.round(xe3) ;
           xe4 = (int)Math.round(xe4) ;
		    
		
           Polygon explosionSquare = new Polygon();		
		
           explosionSquare.addPoint((int)xe1,(int)ye1);
           explosionSquare.addPoint((int)xe2,(int)ye2);
           explosionSquare.addPoint((int)xe3,(int)ye3);
           explosionSquare.addPoint((int)xe4,(int)ye4);	
		
		
           g.setColor(color);
           g.fillPolygon(explosionSquare);
						
    }
}

}
