package tank.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;


/*
* ------------------------------------------------------------------------
* 
*  author  : Harri Isom�ki
*  
*  Class   : ObjectAmmunition
*  
* ------------------------------------------------------------------------
* 
*  Ammunition object is started, when user clicks SPACE tangent.
*	  
*  Every time when ammunition object takes step, it send x/y- coordinates to
*  CollisionDetector by calling synchronized method checkCollision.
*
*  If ammunition object has collides with house object, explosion is 
*  generated.
*  
*
*  Position of ammunition is counted with following formula :
*
*        x = x + incrementX     
*        y = y + incrementX *tan(gunAngle)
*
*
*  When ammunition object is started, ammunition object has to know 
*  coordinates of tank gun and gun angle. Theses values are received from
*  tank object
*             
*-------------------------------------------------------------------------             
*/

public class ObjectAmmunition extends ObjBasicMoveableClass  implements Runnable
{	
   private boolean result= false;
   private ObjectTank objectTank;
	
   private Thread ammunitionThread;
   private double gunAngle,x=-11,y=-51,velX = 1, velY = 1;
	
	
	
   public ObjectAmmunition(double x, double y, int idNumber, Color color,
       ObjBasicCollisionDetector objBasicCollisionDetector,ObjectTank objectTank)
   {
      super(x, y, idNumber, color, objBasicCollisionDetector);		
      this.objectTank = objectTank;		
   }

	
	
	
   public void start() 
   {
      ammunitionThread = new Thread(this);
      ammunitionThread.start();
   }
	
	
	
	
	
	
   private void move() 
   {		
      if(gunAngle > 2*Math.PI){ gunAngle = gunAngle - 2*Math.PI;}
      if(gunAngle < -2*Math.PI){ gunAngle = gunAngle + 2*Math.PI;} 
 				
      if(0 < gunAngle && gunAngle < Math.PI/2 ){ velX =1; }		
      if(Math.PI/2 < gunAngle && gunAngle < 1.5*Math.PI ) { velX = -1;}
      if(1.5 *Math.PI < gunAngle && gunAngle < 2*Math.PI ){ velX = 1;}
		
      if(-2*Math.PI < gunAngle && gunAngle < - 1.5*Math.PI/2 ){ velX =1; }	
      if(-1.5*Math.PI < gunAngle && gunAngle < - Math.PI/2 )  { velX = -1;}		
      if(-Math.PI/2 < gunAngle && gunAngle < 0 ){ velX =1; }
		
		
		
      x = x + velX;		
      velY = velX * Math.tan(gunAngle);
				
      if( velY > 6){velY = 6;}
      if( velY < -6){velY = -6;}
			
      y = y + velY;	
   }
	
	
	
	
   @Override
   public void run() 
   {		
      x = objectTank.getGunX();
      y = objectTank.getGunY();
      y = y -5 ;
      gunAngle = objectTank.getGunAngle();
		
		
      Thread thisThread = Thread.currentThread();


		
      while(ammunitionThread == thisThread)
      {
	  move();
	  checkCollisionDetected();	
		    
	 try 
	 {
	    Thread.sleep(2);
	 }
         catch (InterruptedException e) 
	 {				
            System.out.println("tank loop_" + e.getMessage() + "---" + e.getStackTrace() );
	 }		
     }
		
   }

	
	
   public void checkCollisionDetected()
   {
      if(this.objBasicCollisionDetector.checkCollision(x,y,idNumber))
      {
         result = true;
      }
	
   }

	
	
	
   public void paint(Graphics g)
   {
      if(!result)
      {	
	g.setColor(Color.red);
	g.fillOval((int)x,(int)y,5,5);	
      }
      else
      {
	x=-111;
	y=-111;
      }	
   }
	

}
